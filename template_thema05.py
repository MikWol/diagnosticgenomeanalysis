#!/usr/bin/env python3

"""
Dit is de Doc string
"""

__author__ = 'Mika Wolters', 'Rico ten Brink'

import sys
import argparse


# constanten

# functies
# delivereble 6
def parse_ANNOVAR(filename, columns=[]):
    """
    read ANNOVAR file:
        get header
        print inf per line based on given columns + new line
        print inf per line based on given columns + new line and tab seperated
        use yield to return data
    """

    pass


# deliverble 7

def get_gene_name(gene_name):
    """
    split on ',' for each item,
        replace text between () with '',
        replace NONE with '-'
        replace LOC or LIN with '-'
        if '-' is not the only item remove '-'
    return '/'.join(list)
    """

    pass


# deliverble 9
def dbconnect(bdname, username, password):
    """
    copy from deliverble 9
    make it so when ERROR print(Error conection)
    and exit code
    """
    pass


def determine_gene(cursor):
    """
    'import' from script 9 line 29:30
    return data
    """
    pass


def determine_chromosome(cursor):
    """
    'import' from script 9 line 27:28
    return data
    """
    pass


def parse_to_database():
    """
    'import' from script 9 line 31:50
    """
    pass


def main():
    """
    use argeparse to determine filename, and mariadb credentials
    """
    parse_ANNOVAR('example.txt', [])
    get_gene_name('gene_name')

    return 0


if __name__ == '__main__':
    exitcode = main(sys.argv)
    sys.exit(exitcode)

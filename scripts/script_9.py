#!/usr/bin/python3
import argparse
import getpass
import mariadb


def dbconnect(dbname, username, password):
    try:
        return mariadb.connect(db=dbname, user=username, password=password, host='mariadb', port=3306)
    except mariadb.Error:
        print("Sumting Wong")


def main():
    try:
        args = argparse.ArgumentParser()
        args.add_argument("dbname")
        args.add_argument("username")
        args.add_argument("inputfile")
        argus = args.parse_args()
        dbname = argus.dbname
        username = argus.username
        inputfile = argus.inputfile
        password = getpass.getpass()
        conn = dbconnect(dbname, username, password)
        cur = conn.cursor()
        cur.execute('select * from chromosomes;')
        chromosomes = {row[1]:row[0] for row in cur.fetchall()}
        cur.execute('select * from genes;')
        genes = {row[1]:row[0] for row in cur.fetchall()}
        with open(inputfile) as annovar:
            fl = True
            header = []
            for line in annovar:
                if fl:
                    fl = False
                    header = line.strip('\n').split('\t')
                    continue
                temp_data = line.strip('\n').split('\t')
                chromosome = temp_data[header.index('chromosome')]
                if not chromosome in chromosomes:
                    cur.execute('insert into chromosomes(name) values (\'{}\');'.format(chromosome))
                    cur.execute('select id from chromosomes where name = \'{}\';'.format(chromosome))
                    chromosomes[chromosome] = cur.fetchall()[0][0]
                gene = temp_data[header.index('UCSCKnownGene_Gene')].split('(')[0]
                if not gene in genes:
                    cur.execute('insert into genes(name) values (\'{}\');'.format(gene))
                    cur.execute('select id from genes where name = \'{}\';'.format(gene))
                    genes[gene] = cur.fetchall()[0][0]
                polyphen = temp_data[header.index('LJB2_PolyPhen2_HVAR')]
                polyphen = float(polyphen.split(',')[0]) if polyphen else None
                cur.execute('insert into variants(chromosome, gene, polyphen) values (?, ?, ?);', (chromosomes[chromosome], genes[gene], polyphen))
        conn.commit()
        conn.close()
    except mariadb.Error as e:
        print("Database Error", e)
    except KeyboardInterrupt:
        print("\nExit\n")




if __name__ == '__main__':
    main()
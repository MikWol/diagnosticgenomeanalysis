


def parse_file(data_file, columns = []):
    print(columns)
    with open(data_file) as dfile:
        first_line = dfile.readline().split("\t")
        # header = [first_line[i] for i in range(len(first_line)) if i in columns]
        # print(header)
        for line in dfile:
            line = line.split("\t")
            # print([line[i] for i in range(len(line)) if i in columns])
            d = {first_line[i]: line[i] for i in range(len(line)) if i in columns}
            for item in d.items():
                print(f'{item[0]}: {item[1]}', end=', ')
            # ind = first_line.index("dbsnp138")
            # print(f'\n\tdbsnp: {line[ind]}')
            for item in d.items():
                print(f'\t{item[0]}: {item[1]}')
            print()
            # break


if __name__ == '__main__':
    data_file = 'data/example.txt'
    columns = [10, 11, *range(15, 18), *range(20, 24)]
    parse_file(data_file, columns)
#!/usr/bin/env python3

import argparse

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for parsing and filtering VCF data given a certain variant
allele frequency value.

Deliverable 5
-------------
Make changes to the `parse_vcf_data` function AND the `main` function,
following the instructions preceded with double '##' symbols.

    usage:
        python3 deliverable5.py vcf_file.vcf frequency out_file.vcf

    arguments:
        vcf_file.vcf: the input VCF file, output from the varscan tool
                      frequency: a number (integer) to use as filtering value
        out_file.vcf: name of the output VCF file 

    output:
        a VCF file containing the complete header (comment lines) and
        only the remaining variant positions after filtering.
"""

# METADATA VARIABLES
__author__ = "Marcel Kempenaar"
__status__ = "Template"
__version__ = "2019.d5.v1"

# IMPORT
import sys


def parse_vcf_data(vcf_input_file, frequency, vcf_output_file):
    """ This function reads the input VCF file line by line, skipping the first
    n-header lines. The remaining lines are parsed to filter out variant allele
    frequencies > frequency.
    """

    ## Open the INTPUT VCF file, read the contents line-by-line
    ## Write the first ... comment-lines (header) directly to the output file
    with open(vcf_input_file) as read_file:
        format_index = ""
        for line in read_file:
            line = line.rstrip()
            if not line.startswith("##"):
                if line.startswith("#"):
                    with open(vcf_output_file, "w+") as write_file:
                        print(line, file=write_file)
                        format_index = line.split().index("FORMAT")
                else:
                    format = line.split()[format_index].split(":")
                    freq_index = format.index("FREQ")
                    freq = line.split()[format_index + 1].split(":")[freq_index]
                    freq = float(freq.replace("%", ""))
                    if freq > frequency:
                        with open(vcf_output_file, "a") as write_file:
                            print(line, file=write_file)
    ## Compare the 'FREQ' field with the `frequency` value and write the line
    ## to the output file if FREQ > frequency
    pass

# MAIN
def main(args):
    """ Main function """

    ### INPUT ###
    # Try to read input arguments from the commandline.
    # *After* testing, make sure the program gives proper errors if input is missing

    ## Change the handling of input/ output to using the `argparse` library
    if len(args) > 1:
        parser = argparse.ArgumentParser(description='')
        parser.add_argument('integers', metavar='N', type=int, nargs='+',
                            help='an integer for the accumulator')
        parser.add_argument('--sum', dest='accumulate', action='store_const',
                            const=sum, default=max,
                            help='sum the integers (default: find the max)')

        args = parser.parse_args()
        vcf_file = args[1]
        frequency = args[2]
        out_vcf = args[3]
    else:
        print('Warning, no arguments given, using default values (testing only)...')
        vcf_file = 'data/example.vcf'
        out_vcf = 'data/d5_output.vcf'
        frequency = 30

    # Process the VCF-file
    parse_vcf_data(vcf_file, frequency, out_vcf)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
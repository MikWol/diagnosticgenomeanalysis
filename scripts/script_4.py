#!/usr/bin/env python3
import sys
import csv
import datetime


# FUNCTIONS

def parse_bed(filename):
    bed_dict = {}
    with open(filename) as read_file:
        for line in read_file:
            temp_data = line.strip('\n').split('\t')
            key = temp_data[0]
            bed_dict.setdefault(key, [])
            bed_dict[key].append(tuple([int(x) if x.isnumeric() else x for x in temp_data[1:]]))
    return bed_dict


def parse_pileup(filename, bed_dict={}):
    coverage_dict = {}
    bd = {'chr' + key: values for key, values in bed_dict.items()}
    print(bed_dict)
    with open(filename) as read_file:
        # coverage_dict = {key: [int(line.strip('\n').split('\t')[3]) for line in read_file if line.strip('\n').split('\t')[3] == 'chr' + line.strip('\n').split('\t')[3] and [value for value in values if int( value[ 1]) > int( line.strip( '\n').split( '\t')[ 1]) > int( value[ 0])]] for key, values in bed_dict.items()}
        # Dit hierboven is hetzelfde als dit hieronder

        for line in read_file:
            temp_data = line.strip('\n').split('\t')
            vals = bd.get(temp_data[0], None)
            print(temp_data)
            # print(vals)
            if vals:
                position = temp_data[1]
                chromosome = None
                for value in vals:
                    if int(value[1]) > int(position) > int(value[0]):
                        chromosome = value[2]
                        break
                if chromosome:
                    coverage = int(temp_data[3])
                    coverage_dict.setdefault(chromosome, [])
                    coverage_dict[chromosome].append(coverage)
    print(coverage_dict, datetime.datetime.now())
    return coverage_dict


def calculate_mapping_coverage(coverage_dict):
    """ Function to calculate all coverage statistics on a per-gene basis
        and store this in a list.
        Note: this function is taken from deliverable 5 and slightly modified
    """
    statistics = []
    for gene_name, gene_coverage in coverage_dict.items():
        total_coverage = 0
        for coverage in gene_coverage:
            total_coverage += coverage
        statistics.append(tuple([gene_name, len(gene_coverage), total_coverage / len(gene_coverage),
                                 len([bad_coverage for bad_coverage in gene_coverage if bad_coverage < 30])]))
    return statistics


def save_coverage_statistics(coverage_file, coverage_statistics):
    """ Writes coverage data to a tabular file using Python's
        csv library: https://docs.python.org/3/library/csv.html#csv.writer
    """
    with open(coverage_file, "w+", newline='') as write_file:
        spamwriter = csv.writer(write_file, delimiter=' ',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for row in coverage_statistics:
            spamwriter.writerow(row)


# MAIN
def main(args):
    """ Main function connecting all functions
        Note: the 'is None' checks that are done are only
        necessary for this program to run without error if
        not all functions are completed.
    """

    ### INPUT ###
    # Try to read input en output filenames from the commandline. Use defaults if
    # they are missing and warn if the extensions are 'wrong'.
    silence = False
    print(datetime.datetime.now())
    if len(args) > 1:
        if args[1] == '-s':
            args = args[1:]
            silence = True
    if len(args) > 1:
        bed_file = args[1]
        if not bed_file.lower().endswith('.bed'):
            print('Warning: given BED file does not have a ".bed" extension.')
        pileup_file = args[2]
        if not pileup_file.lower().endswith('.pileup'):
            print('Warning: given pileup file does not have a ".pileup" extension.')
        output_file = args[3]
    else:
        bed_file = 'data/example.bed'
        pileup_file = 'data/Galaxy20-[MPileup_on_data_19].pileup'
        output_file = 'data/d4_output.csv'
    if not silence:
        print('Reading BED data from', bed_file)
    bed_data = parse_bed(bed_file)
    if not silence:
        if bed_data is None:
            print('No BED-data read...')
        else:
            print('\t> A total of', len(bed_data), 'lines have been read.\n')

        print('Reading pileup data from', pileup_file)
    pileup_data = parse_pileup(pileup_file, bed_dict=bed_data)
    if not silence:
        if pileup_data is None:
            print('No Pileup-data read...')
        else:
            print('\t> A total of', len(pileup_data), 'lines have been read.\n')

        print('Calculating coverage statistics...')
    print(pileup_data)
    coverage_statistics = calculate_mapping_coverage(pileup_data)
    if not silence:
        if coverage_statistics is None:
            print('No coverage statistics calculated!')
        else:
            print('\t> Statistics for', len(coverage_statistics), 'genes have been calculated.\n')

        # STEP 6: Write output data
        print('Writing the coverage statistics to', output_file)
    if coverage_statistics is None:
        if not silence:
            print('Nothing to write, quitting...')
    else:
        save_coverage_statistics(output_file, coverage_statistics)
        if not silence:
            from pathlib import Path
            csv_file_check = Path(output_file)
            if csv_file_check.is_file():
                print('\t> CSV file created, program finished.')
            else:
                print('\tCSV file', output_file, 'does not exist!')
    print(datetime.datetime.now())
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
